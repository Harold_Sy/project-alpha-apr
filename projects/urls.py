from django.urls import path
from projects.views import (
    projects_list_view,
    project_tasks,
    create_project_view,
)


urlpatterns = [
    path("", projects_list_view, name="list_projects"),
    path("<int:id>/", project_tasks, name="show_project"),
    path("create/", create_project_view, name="create_project"),
]
