from django import forms
from projects.models import Project


class Project_Form(forms.ModelForm):
    class Meta:
        model = Project
        fields = (
            "name",
            "description",
            "owner",
        )
