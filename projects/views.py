from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import Project_Form


@login_required
def projects_list_view(request):
    projects = Project.objects.filter(owner=request.user)
    context = {"projects": projects}
    return render(request, "projects/list.html", context)


def project_tasks(request, id):
    project = get_object_or_404(Project, pk=id)
    tasks = project.tasks.all()
    context = {
        "project": project,
        "tasks": tasks,
    }
    return render(request, "projects/details.html", context)


@login_required
def create_project_view(request):
    if request.method == "POST":
        form = Project_Form(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.save()
            return redirect("list_projects")
    else:
        form = Project_Form()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
